package fr.cda.springbootentreprise.service;

import fr.cda.springbootentreprise.model_entity.SocieteModeleEntity;
import fr.cda.springbootentreprise.repository.SocieteRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Service pour la gestion des opérations liées aux sociétés.
 */
@Service
@RequiredArgsConstructor
public class SocieteService {

    // Repository pour les opérations liées aux sociétés
    private final SocieteRepository societeRepository;


    /**
     * Supprime une société en utilisant son identifiant.
     *
     * @param id L'identifiant de la société à supprimer.
     * @return ResponseEntity avec un message approprié et le statut HTTP correspondant.
     */
    public ResponseEntity<String> societeDelete(Long id) {
        // Recherche de la société par son identifiant
        Optional<SocieteModeleEntity> societe = societeRepository.findById(id);

        // Vérifie si la société existe
        if (societe.isPresent()) {
            // Supprime la société
            societeRepository.deleteById(id);
            // Retourne une réponse avec le message de succès et le statut OK
            return new ResponseEntity<>("La société est supprimée", HttpStatus.OK);
        } else {
            // Société non trouvée, retourne une réponse avec un message approprié et le statut NOT_FOUND
            return new ResponseEntity<>("Société non trouvée", HttpStatus.NOT_FOUND);
        }
    }
}
