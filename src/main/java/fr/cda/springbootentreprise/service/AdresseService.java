package fr.cda.springbootentreprise.service;

import fr.cda.springbootentreprise.model_entity.AdresseModeleEntity;
import fr.cda.springbootentreprise.repository.AdresseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Service pour la gestion des opérations liées aux adresses.
 */
@Service
@RequiredArgsConstructor
public class AdresseService {

    // Repository pour les opérations liées aux adresses
    private final AdresseRepository adresseRepository;

    /**

     * Supprime une adresse en utilisant son identifiant.
     *
     * @param id L'identifiant de l'adresse à supprimer.
     * @return ResponseEntity avec un message approprié et le statut HTTP correspondant.
     */
    public ResponseEntity<String> adresseDelete(Long id) {
        // Recherche de l'adresse par son identifiant
        Optional<AdresseModeleEntity> adresse = adresseRepository.findById(id);

        // Vérifie si l'adresse existe
        if (adresse.isPresent()) {
            // Supprime l'adresse
            adresseRepository.deleteById(id);
            // Retourne une réponse avec le message de succès et le statut OK
            return new ResponseEntity<>("L'adresse est supprimée", HttpStatus.OK);
        } else {
            // Adresse non trouvée, retourne une réponse avec un message approprié et le statut NOT_FOUND
            return new ResponseEntity<>("Adresse non trouvée", HttpStatus.NOT_FOUND);
        }
    }
}
