package fr.cda.springbootentreprise.service;

import fr.cda.springbootentreprise.model_entity.ServiceModeleEntity;
import fr.cda.springbootentreprise.repository.ServiceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Service pour la gestion des opérations liées aux services.
 */
@Service
@RequiredArgsConstructor
public class ServiceService {

    // Repository pour les opérations liées aux services
    private final ServiceRepository serviceRepository;


    /**
     * Supprime un service en utilisant son identifiant.
     *
     * @param id L'identifiant du service à supprimer.
     * @return ResponseEntity avec un message approprié et le statut HTTP correspondant.
     */
    public ResponseEntity<String> serviceDelete(Long id) {
        // Recherche du service par son identifiant
        Optional<ServiceModeleEntity> service = serviceRepository.findById(id);

        // Vérifie si le service existe
        if (service.isPresent()) {
            // Supprime le service
            serviceRepository.deleteById(id);
            // Retourne une réponse avec le message de succès et le statut OK
            return new ResponseEntity<>("Le service est supprimé", HttpStatus.OK);
        } else {
            // Service non trouvé, retourne une réponse avec un message approprié et le statut NOT_FOUND
            return new ResponseEntity<>("Service non trouvé", HttpStatus.NOT_FOUND);
        }
    }
}
