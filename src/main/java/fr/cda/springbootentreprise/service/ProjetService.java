package fr.cda.springbootentreprise.service;

import fr.cda.springbootentreprise.exception.ProjetNotFoundException;
import fr.cda.springbootentreprise.exception.SalarieAppartientProjetException;
import fr.cda.springbootentreprise.exception.SalarieNotFoundException;
import fr.cda.springbootentreprise.model_entity.ProjetModeleEntity;
import fr.cda.springbootentreprise.model_entity.SalarieModeleEntity;
import fr.cda.springbootentreprise.repository.ProjetRepository;
import fr.cda.springbootentreprise.repository.SalarieRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Service pour la gestion des opérations liées aux projets.
 */
@Service
@RequiredArgsConstructor
public class ProjetService {

    // Repository pour les opérations liées aux projets
    @Autowired
    private final ProjetRepository projetRepository;

    // Repository pour les opérations liées aux salariés
    @Autowired
    private final SalarieRepository salarieRepository;


    /**
     * Retourne la liste des salariés qui font partie d'un projet selon son id.
     *
     * @param idProjet Id du projet
     * @return Une liste avec tous les salariés du projet
     * @throws ProjetNotFoundException Si le projet n'est pas trouvé
     */
    public List<SalarieModeleEntity> filtreSalarie(Long idProjet) {
        Optional<ProjetModeleEntity> projet = projetRepository.findById(idProjet);
        if (projet.isPresent()) {
            return projet.get().getSalaries();
        } else {
            throw new ProjetNotFoundException("Le projet avec l'ID " + idProjet + " n'a pas été trouvé.");
        }
    }

    /**
     * Fonction qui ajoute un salarié à un projet.
     *
     * @param salarie Le salarié à ajouter.
     * @param projet  Le projet auquel ajouter le salarié.
     * @return ResponseEntity avec un message de confirmation et le statut HTTP correspondant.
     * @throws SalarieAppartientProjetException Si le salarié est déjà associé à ce projet.
     */
    public ResponseEntity<String> addSalarieToProjet(SalarieModeleEntity salarie, ProjetModeleEntity projet) throws Exception {
        List<SalarieModeleEntity> salaries = projet.getSalaries();
        if (salaries.contains(salarie)) {
            throw new SalarieAppartientProjetException();
        } else {
            salaries.add(salarie);
            projet.setSalaries(salaries);
            projetRepository.save(projet);
            return new ResponseEntity<>("Le salarié a été ajouté au projet", HttpStatus.OK);
        }
    }

    /**
     * Fonction qui enlève un salarié d'un projet.
     *
     * @param salarie Le salarié à enlever.
     * @param projet  Le projet duquel enlever le salarié.
     * @return ResponseEntity avec un message de confirmation et le statut HTTP correspondant.
     * @throws SalarieNotFoundException Si le projet ne contient pas le salarié.
     */
    public ResponseEntity<String> removeSalarieFromProjet(SalarieModeleEntity salarie, ProjetModeleEntity projet) throws Exception {
        List<SalarieModeleEntity> salaries = projet.getSalaries();
        if (salaries.contains(salarie)) {
            salaries.remove(salarie);
            projet.setSalaries(salaries);
            projetRepository.save(projet);
            return new ResponseEntity<>("Le salarié a été retiré du projet", HttpStatus.OK);
        } else {
            throw new SalarieNotFoundException();
        }
    }
}
