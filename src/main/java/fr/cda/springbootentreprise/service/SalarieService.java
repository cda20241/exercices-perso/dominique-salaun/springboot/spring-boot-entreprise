package fr.cda.springbootentreprise.service;

import fr.cda.springbootentreprise.model_entity.SalarieModeleEntity;
import fr.cda.springbootentreprise.repository.SalarieRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Service pour la gestion des opérations liées aux salariés.
 */

@RequiredArgsConstructor
@Service
public class SalarieService {

    // Repository pour les opérations liées aux salariés

    private  SalarieRepository salarieRepository;


    /**
     * Supprime un salarié en utilisant son identifiant.
     *
     * @param id L'identifiant du salarié à supprimer.
     * @return ResponseEntity avec un message approprié et le statut HTTP correspondant.
     */
    public ResponseEntity<String> salarieDelete(Long id) {
        // Recherche du salarié par son identifiant
        Optional<SalarieModeleEntity> salarie = salarieRepository.findById(id);

        // Vérifie si le salarié existe
        if (salarie.isPresent()) {
            // Supprime le salarié
            salarieRepository.deleteById(id);
            // Retourne une réponse avec le message de succès et le statut OK
            return new ResponseEntity<>("Le salarié est supprimé", HttpStatus.OK);
        } else {
            // Salarié non trouvé, retourne une réponse avec un message approprié et le statut NOT_FOUND
            return new ResponseEntity<>("Salarie non trouvé", HttpStatus.NOT_FOUND);
        }
    }
}
