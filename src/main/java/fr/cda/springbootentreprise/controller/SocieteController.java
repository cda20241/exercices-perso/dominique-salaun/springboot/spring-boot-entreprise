package fr.cda.springbootentreprise.controller;

import fr.cda.springbootentreprise.model_entity.SocieteModeleEntity;
import fr.cda.springbootentreprise.repository.SocieteRepository;
import fr.cda.springbootentreprise.service.SocieteService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/societe")
@RequiredArgsConstructor
public class SocieteController {

    private final SocieteRepository societeRepository;
    private final SocieteService societeService;

    // Affiche toutes les sociétés de la base de données
    @GetMapping("/all")
    public ResponseEntity<List<SocieteModeleEntity>> getAllSociete() {
        List<SocieteModeleEntity> societes = societeRepository.findAll();
        return ResponseEntity.ok(societes);
    }

    // Affiche une société selon son id
    @GetMapping("/{id}")
    public ResponseEntity<SocieteModeleEntity> getSociete(@PathVariable Long id) {
        Optional<SocieteModeleEntity> societe = societeRepository.findById(id);
        return societe.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    // Ajoute une société à la base de données
    @PostMapping("")
    public ResponseEntity<SocieteModeleEntity> addSociete(@RequestBody SocieteModeleEntity societe) {
        societe.setIdSociete(null);
        SocieteModeleEntity savedSociete = societeRepository.save(societe);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedSociete);
    }

    // Met à jour une société dans la base de données
    @PutMapping("/{id}")
    public ResponseEntity<SocieteModeleEntity> updateSociete(@PathVariable Long id, @RequestBody SocieteModeleEntity societe) {
        societe.setIdSociete(id);
        SocieteModeleEntity updatedSociete = societeRepository.save(societe);
        return ResponseEntity.ok(updatedSociete);
    }

    // Supprime une société de la base de données
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteSociete(@PathVariable Long id) {
        return societeService.societeDelete(id);
    }
}
