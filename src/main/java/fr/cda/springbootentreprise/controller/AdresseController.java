package fr.cda.springbootentreprise.controller;

import fr.cda.springbootentreprise.model_entity.AdresseModeleEntity;
import fr.cda.springbootentreprise.repository.AdresseRepository;
import fr.cda.springbootentreprise.service.AdresseService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/adresse")
@RequiredArgsConstructor
public class AdresseController {

    private final AdresseRepository adresseRepository;
    private final AdresseService adresseService;

    // Récupérer toutes les adresses
    @GetMapping("/all")
    public ResponseEntity<List<AdresseModeleEntity>> getAllAdresse() {
        List<AdresseModeleEntity> adresses = adresseRepository.findAll();
        return ResponseEntity.ok(adresses);
    }

    // Récupérer une adresse par son ID
    @GetMapping("/{id}")
    public ResponseEntity<?> getAdresse(@PathVariable Long id) {
        Optional<AdresseModeleEntity> adresse = adresseRepository.findById(id);
        return adresse.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    // Ajouter une nouvelle adresse
    @PostMapping("")
    public ResponseEntity<?> addAdresse(@RequestBody AdresseModeleEntity adresse) {
        adresse.setIdAdresse(null);
        AdresseModeleEntity savedAdresse = adresseRepository.save(adresse);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedAdresse);
    }

    // Ajouter plusieurs adresses
    @PostMapping("/addAddresses")
    public ResponseEntity<List<?>> addAddresses(@RequestBody List<AdresseModeleEntity> addresses) {
        List<AdresseModeleEntity> savedAddresses = adresseRepository.saveAll(addresses);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedAddresses);
    }

    // Mettre à jour une adresse par son ID
    @PutMapping("/{id}")
    public ResponseEntity<?> updateAdresse(@PathVariable Long id, @RequestBody AdresseModeleEntity adresse) {
        adresse.setIdAdresse(id);
        AdresseModeleEntity updatedAdresse = adresseRepository.save(adresse);
        return ResponseEntity.ok(updatedAdresse);
    }

    // Supprimer une adresse par son ID
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteAdresse(@PathVariable Long id) {
        return adresseService.adresseDelete(id);
    }
}
