package fr.cda.springbootentreprise.controller;

import fr.cda.springbootentreprise.model_entity.ServiceModeleEntity;
import fr.cda.springbootentreprise.repository.ServiceRepository;
import fr.cda.springbootentreprise.service.ServiceService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/service")
@RequiredArgsConstructor
public class ServiceController {

    private final ServiceRepository serviceRepository;
    private final ServiceService serviceService;

    // Affiche tous les services de la base de données
    @GetMapping("/all")
    public ResponseEntity<List<ServiceModeleEntity>> getAllService() {
        List<ServiceModeleEntity> services = serviceRepository.findAll();
        return ResponseEntity.ok(services);
    }

    // Affiche un service selon son id
    @GetMapping("/{id}")
    public ResponseEntity<ServiceModeleEntity> getService(@PathVariable Long id) {
        Optional<ServiceModeleEntity> service = serviceRepository.findById(id);
        return service.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    // Ajoute un service à la base de données
    @PostMapping("")
    public ResponseEntity<ServiceModeleEntity> addService(@RequestBody ServiceModeleEntity service) {
        service.setIdService(null);
        ServiceModeleEntity savedService = serviceRepository.save(service);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedService);
    }

    // Met à jour un service dans la base de données
    @PutMapping("/{id}")
    public ResponseEntity<ServiceModeleEntity> updateService(@PathVariable Long id, @RequestBody ServiceModeleEntity service) {
        service.setIdService(id);
        ServiceModeleEntity updatedService = serviceRepository.save(service);
        return ResponseEntity.ok(updatedService);
    }

    // Supprime un service de la base de données
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteService(@PathVariable Long id) {
        return serviceService.serviceDelete(id);
    }
}
