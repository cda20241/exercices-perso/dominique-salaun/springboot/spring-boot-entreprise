package fr.cda.springbootentreprise.controller;

import fr.cda.springbootentreprise.model_entity.ProjetModeleEntity;
import fr.cda.springbootentreprise.model_entity.SalarieModeleEntity;
import fr.cda.springbootentreprise.repository.ProjetRepository;
import fr.cda.springbootentreprise.repository.SalarieRepository;
import fr.cda.springbootentreprise.service.ProjetService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/projet")
@RequiredArgsConstructor
public class ProjetController {

    @Autowired
    private final ProjetRepository projetRepository;

    @Autowired
    private final ProjetService projetService;

    @Autowired
    private final SalarieRepository salarieRepository;

    // Affiche tous les projets de la base de données
    @GetMapping("/all")
    public List<ProjetModeleEntity> getAllProjet(){
        return projetRepository.findAll();
    }

    // Affiche un projet selon son id
    @GetMapping("/{id}")
    public ProjetModeleEntity getProjet(@PathVariable Long id){
        Optional<ProjetModeleEntity> projet = projetRepository.findById(id);
        return projet.orElse(null);
    }

    // Ajoute un projet à la base de données
    @PostMapping("/add")
    public ProjetModeleEntity addProjet(@RequestBody ProjetModeleEntity projet){
        projet.setIdProjet(null);
        return projetRepository.save(projet);
    }

    // Met à jour un projet dans la base de données
    @PutMapping("/{id}")
    public ProjetModeleEntity updateProjet(@PathVariable Long id, @RequestBody ProjetModeleEntity projet){
        projet.setIdProjet(id);
        return projetRepository.save(projet);
    }

    // Supprime un projet de la base de données
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteProjet(@PathVariable Long id){
        Optional<ProjetModeleEntity> projet = projetRepository.findById(id);
        if(projet.isPresent()){
            projetRepository.deleteById(id);
            return new ResponseEntity<>("Projet Supprimé", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Projet non trouvé", HttpStatus.NOT_FOUND);
        }
    }

    // Filtre les salariés d'un projet
    @GetMapping("/filtre/{id}")
    public List<SalarieModeleEntity> filtreSalarie(@PathVariable Long id){
        return projetService.filtreSalarie(id);
    }

    // Ajoute un salarié à un projet
    @GetMapping("/addSalarie/{id_salarie}/{id_projet}")
    public ResponseEntity<String> addSalarie(@PathVariable Long id_salarie, @PathVariable Long id_projet) throws Exception {
        Optional<SalarieModeleEntity> salarie = salarieRepository.findById(id_salarie);
        Optional<ProjetModeleEntity> projet = projetRepository.findById(id_projet);
        return projetService.addSalarieToProjet(salarie.get(), projet.get());
    }

    // Retire un salarié d'un projet
    @GetMapping("/removeSalarie/{id_salarie}/{id_projet}")
    public ResponseEntity<String> removeSalarie(@PathVariable Long id_salarie, @PathVariable Long id_projet) throws Exception {
        Optional<SalarieModeleEntity> salarie = salarieRepository.findById(id_salarie);
        Optional<ProjetModeleEntity> projet = projetRepository.findById(id_projet);
        return projetService.removeSalarieFromProjet(salarie.get(), projet.get());
    }
}
