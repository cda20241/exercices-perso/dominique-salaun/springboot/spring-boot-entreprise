package fr.cda.springbootentreprise.controller;

import fr.cda.springbootentreprise.model_entity.SalarieModeleEntity;
import fr.cda.springbootentreprise.model_entity.ServiceModeleEntity;
import fr.cda.springbootentreprise.repository.SalarieRepository;
import fr.cda.springbootentreprise.service.SalarieService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/salarie")
@RequiredArgsConstructor
public class SalarieController {

    private final SalarieRepository salarieRepository;
    private final SalarieService salarieService;

    // Affiche tous les salariés de la base de données
    @GetMapping("/all")
    public ResponseEntity<List<SalarieModeleEntity>> getAllSalarie() {
        List<SalarieModeleEntity> salaries = salarieRepository.findAll();
        return ResponseEntity.ok(salaries);
    }

    // Affiche un salarié selon son id
    @GetMapping("/{id}")
    public ResponseEntity<SalarieModeleEntity> getSalarie(@PathVariable Long id) {
        Optional<SalarieModeleEntity> salarie = salarieRepository.findById(id);
        return salarie.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
    }

    // Ajoute un salarié à la base de données
    @PostMapping("")
    public ResponseEntity<SalarieModeleEntity> addSalarie(@RequestBody SalarieModeleEntity salarie) {
        salarie.setIdSalarie(null);

        // Gestion de l'objet ServiceModeleEntity lors de l'ajout d'un salarié
        ServiceModeleEntity service = salarie.getService();
        if (service != null) {
            Long idService = service.getIdService();
            // Faites ce que vous devez faire avec l'id du service
        }

        SalarieModeleEntity savedSalarie = salarieRepository.save(salarie);
        return ResponseEntity.status(HttpStatus.CREATED).body(savedSalarie);
    }

    // Met à jour un salarié dans la base de données
    @PutMapping("/{id}")
    public ResponseEntity<SalarieModeleEntity> updateSalarie(@PathVariable Long id, @RequestBody SalarieModeleEntity salarie) {
        salarie.setIdSalarie(id);
        SalarieModeleEntity updatedSalarie = salarieRepository.save(salarie);
        return ResponseEntity.ok(updatedSalarie);
    }

    // Supprime un salarié de la base de données
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteSalarie(@PathVariable Long id) {
        return salarieService.salarieDelete(id);
    }
}
