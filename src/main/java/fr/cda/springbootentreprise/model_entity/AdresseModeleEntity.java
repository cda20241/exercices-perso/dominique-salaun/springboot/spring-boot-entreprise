package fr.cda.springbootentreprise.model_entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * Entité représentant une adresse dans la base de données.
 */
@Data
@RequiredArgsConstructor

@Entity(name = "Adresse")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idAdresse")
public class AdresseModeleEntity {

    // Identifiant de l'adresse (clé primaire auto-générée)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_adresse")
    private Long idAdresse;

    // Rue de l'adresse (non nullable)
    @Column(name = "Rue", nullable = false)
    private String rue;

    // Ville de l'adresse (non nullable)
    @Column(name = "Ville", nullable = false)
    private String ville;

    // Code postal de l'adresse (non nullable)
    @Column(name = "Code_postal", nullable = false)
    private Integer codePostal;

    // Pays de l'adresse (non nullable)
    @Column(name = "Pays", nullable = false)
    private String pays;
}
