package fr.cda.springbootentreprise.model_entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * Entité représentant un service dans la base de données.
 */
@Data

@RequiredArgsConstructor

@Entity(name = "Service")
public class ServiceModeleEntity {

    // Identifiant du service (clé primaire auto-générée)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_service")
    private Long idService;

    // Nom du service (non nullable)
    @Column(name = "Nom_Service", nullable = false)
    private String nom;
}
