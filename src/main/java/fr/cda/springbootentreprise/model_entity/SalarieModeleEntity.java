package fr.cda.springbootentreprise.model_entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

/**
 * Entité représentant un salarié dans la base de données.
 */
@Data

@RequiredArgsConstructor

@Entity(name = "Salarie")
public class SalarieModeleEntity {

    // Identifiant du salarié (clé primaire auto-générée)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_salarie")
    private Long idSalarie;

    // Nom du salarié
    @Column(name = "nom")
    private String nom;

    // Prénom du salarié
    @Column(name = "prenom")
    private String prenom;

    // Relation Many-to-One avec la classe ServiceModeleEntity (un salarié appartient à un service)
    @ManyToOne
    @JoinColumn(name = "fk_service", referencedColumnName = "id_service")
    private ServiceModeleEntity service;

    // Relation One-to-One avec la classe AdresseModeleEntity (un salarié a une adresse)
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "fk_adresse", referencedColumnName = "id_adresse", unique = true)
    private AdresseModeleEntity adresse;

    // Relation Many-to-One avec la classe SocieteModeleEntity (un salarié appartient à une société)
    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "fk_societe", unique = true)
    private SocieteModeleEntity societe;

    // Relation Many-to-Many avec la classe ProjetEntity (un salarier peut avoir plusieurs projet)
    @ManyToMany(mappedBy = "salaries")
    private List<ProjetModeleEntity> projet;

}
