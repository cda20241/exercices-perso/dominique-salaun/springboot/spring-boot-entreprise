package fr.cda.springbootentreprise.model_entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * Entité représentant un projet dans la base de données.
 */
@Data

@RequiredArgsConstructor

@Entity(name = "Projet")
public class ProjetModeleEntity {

    // Identifiant du projet (clé primaire auto-générée)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_projet")
    private Long idProjet;

    // Nom du projet (non nullable)
    @Column(name = "nom", nullable = false)
    private String nom;

    // Relation Many-to-Many avec la classe SalarieModeleEntity (un projet peut avoir plusieurs salariés)
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "Projet_en_Court",
            joinColumns = @JoinColumn(name = "fk_projet", referencedColumnName = "id_projet"),
            inverseJoinColumns = @JoinColumn(name = "fk_salarie", referencedColumnName = "id_salarie"))
    private List<SalarieModeleEntity> salaries = new ArrayList<>();
}
