package fr.cda.springbootentreprise.model_entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * Entité représentant une société dans la base de données.
 */
@Data

@RequiredArgsConstructor

@Entity(name = "Societe")
public class SocieteModeleEntity {

    // Identifiant de la société (clé primaire auto-générée)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_societe")
    private Long idSociete;

    // Nom de la société
    @Column(name = "nom")
    private String nom;
}
