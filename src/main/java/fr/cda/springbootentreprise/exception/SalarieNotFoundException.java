package fr.cda.springbootentreprise.exception;

public class SalarieNotFoundException extends RuntimeException{
    public SalarieNotFoundException(){
        super("Salarie non trouver");
    }

}
