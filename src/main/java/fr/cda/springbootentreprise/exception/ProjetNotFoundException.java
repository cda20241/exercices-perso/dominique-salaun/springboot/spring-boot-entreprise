package fr.cda.springbootentreprise.exception;

public class ProjetNotFoundException extends RuntimeException {
    public ProjetNotFoundException(String message){
        super(message);
    }
}
