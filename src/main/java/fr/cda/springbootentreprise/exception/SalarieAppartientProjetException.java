package fr.cda.springbootentreprise.exception;

public class SalarieAppartientProjetException extends RuntimeException {
    public SalarieAppartientProjetException(){
        super("Le salarie appartient deja au projet");
    }
}
