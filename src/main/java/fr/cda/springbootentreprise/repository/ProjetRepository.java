package fr.cda.springbootentreprise.repository;

import fr.cda.springbootentreprise.model_entity.ProjetModeleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface Repository pour les opérations liées aux projets dans la base de données.
 * Elle étend JpaRepository pour bénéficier des fonctionnalités CRUD de Spring Data JPA.
 */
public interface ProjetRepository extends JpaRepository<ProjetModeleEntity, Long> {
    // Aucune méthode supplémentaire n'est nécessaire ici, car JpaRepository fournit déjà des méthodes pour CRUD.
}
