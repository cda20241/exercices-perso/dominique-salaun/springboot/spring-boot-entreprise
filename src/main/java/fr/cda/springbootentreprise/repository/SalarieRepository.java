package fr.cda.springbootentreprise.repository;

import fr.cda.springbootentreprise.model_entity.SalarieModeleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Interface Repository pour les opérations liées aux salariés dans la base de données.
 * Elle étend JpaRepository pour bénéficier des fonctionnalités CRUD de Spring Data JPA.
 */
public interface SalarieRepository extends JpaRepository<SalarieModeleEntity, Long> {
    // Aucune méthode supplémentaire n'est nécessaire ici, car JpaRepository fournit déjà des méthodes pour CRUD.
}
