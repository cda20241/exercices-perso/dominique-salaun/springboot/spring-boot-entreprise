package fr.cda.springbootentreprise;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Classe principale pour démarrer l'application Spring Boot.
 */
@SpringBootApplication
public class SpringBootEntrepriseApplication implements ApplicationRunner {

	/**
	 * Point d'entrée de l'application.
	 *
	 * @param args Les arguments de la ligne de commande (non utilisés dans cet exemple).
	 */
	public static void main(String[] args) {
		// Lance l'application Spring Boot
		SpringApplication.run(SpringBootEntrepriseApplication.class, args);
	}

	/**
	 * Méthode exécutée au démarrage de l'application.
	 *
	 * @param args Les arguments de l'application.
	 * @throws Exception En cas d'erreur lors de l'exécution.
	 */
	@Override
	public void run(ApplicationArguments args) throws Exception {
		// Spécifiez l'URL Swagger
		String swaggerUrl = "http://localhost:8080/swagger-ui/index.html";

		// Affiche un message dans le terminal avec le lien vers Swagger
		System.out.println("Ouvrez l'URL Swagger dans votre navigateur : " + swaggerUrl);

		// Ouvre l'URL Swagger dans le navigateur par défaut
		try {
			openWebpage(new URI(swaggerUrl));
		} catch (IOException | URISyntaxException e) {
			System.out.println("Impossible d'ouvrir l'URL Swagger. Veuillez le faire manuellement : " + swaggerUrl);
		}
	}

	/**
	 * Ouvre une URL dans le navigateur par défaut.
	 *
	 * @param uri L'URI à ouvrir.
	 * @throws IOException En cas d'erreur d'entrée/sortie.
	 * @throws URISyntaxException En cas d'erreur de syntaxe d'URI.
	 */
	private void openWebpage(URI uri) throws IOException, URISyntaxException {
		if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
			Desktop.getDesktop().browse(uri);
		}
	}
}
