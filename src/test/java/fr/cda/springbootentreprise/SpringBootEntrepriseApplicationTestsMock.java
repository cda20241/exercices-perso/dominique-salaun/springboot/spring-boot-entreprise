package fr.cda.springbootentreprise;


import fr.cda.springbootentreprise.exception.ProjetNotFoundException;
import fr.cda.springbootentreprise.model_entity.ProjetModeleEntity;
import fr.cda.springbootentreprise.model_entity.SalarieModeleEntity;
import fr.cda.springbootentreprise.repository.ProjetRepository;
import fr.cda.springbootentreprise.service.ProjetService;
import org.junit.jupiter.api.*;
import org.mockito.*;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@SpringBootTest
public class SpringBootEntrepriseApplicationTestsMock {


    @InjectMocks
    private ProjetService projetService;
    @Mock
    private ProjetRepository projetRepository;



    /**
     * Teste la méthode filtreSalarie du service ProjetService.
     */
    @Test
    @Transactional
    void testFiltreSalarie() {
        //grace a Mock on peux emuler une base de donné de test pour automatiser et isoler
        // Créez un projet fictif pour le test
        ProjetModeleEntity projetFictif = new ProjetModeleEntity();
        projetFictif.setIdProjet(1L);
        projetFictif.setNom("Nom Projet Fictif 1");

        // Créez une liste fictive de salariés
        List<SalarieModeleEntity> salariesFictifs = new ArrayList<>();
        SalarieModeleEntity s1= new SalarieModeleEntity();
        s1.setNom("Nom Salarie 1");
        s1.setPrenom("Prenom Salarie 1");
        salariesFictifs.add(s1);

        // on ajoute le salarier a la liste projet fictif
        projetFictif.setSalaries(salariesFictifs);

        //on ajoute la list salaries a projet repository

        when(projetRepository.findById(1L)).thenReturn(Optional.of(projetFictif));

        List<SalarieModeleEntity> result = projetService.filtreSalarie(1L);

        assertEquals("Nom Salarie 1", result.get(0).getNom(),"erreur du test equal ");


        // Testez avec un projet inexistant
        assertThrows(ProjetNotFoundException.class, () -> projetService.filtreSalarie(99L),"erreur du test Throws");
    }


}
