package fr.cda.springbootentreprise;

import fr.cda.springbootentreprise.controller.ProjetController;
import fr.cda.springbootentreprise.exception.ProjetNotFoundException;
import fr.cda.springbootentreprise.exception.SalarieAppartientProjetException;
import fr.cda.springbootentreprise.exception.SalarieNotFoundException;
import fr.cda.springbootentreprise.model_entity.ProjetModeleEntity;
import fr.cda.springbootentreprise.model_entity.SalarieModeleEntity;
import fr.cda.springbootentreprise.repository.ProjetRepository;
import fr.cda.springbootentreprise.service.ProjetService;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

/**
 * Classe de tests pour l'application Spring Boot.
 * Assurez-vous d'avoir des commentaires détaillés et des docstrings conformément à vos spécifications.
 */
@SpringBootTest
class SpringBootEntrepriseApplicationTests {

	@Autowired
	private ProjetService projetService;

	@Autowired
	private ProjetRepository projetRepository;
	@Autowired
	private ProjetController projetController;


	/**
	 * Teste la méthode filtreSalarie du service ProjetService.
	 */
	@Test
	@Transactional
	void testFiltreSalarie() {
		// drop la base de données avant de tester sans Mock test pas isoler et pas automatiser
		// Créez un projet fictif pour le test
		ProjetModeleEntity projetFictif = new ProjetModeleEntity();
		projetFictif.setIdProjet(1L);
		projetFictif.setNom("Nom Projet Fictif 1");

		// Créez une liste fictive de salariés
		List<SalarieModeleEntity> salariesFictifs = new ArrayList<>();
		SalarieModeleEntity s1= new SalarieModeleEntity();
		s1.setNom("Nom Salarie 1");
		s1.setPrenom("Prenom Salarie 1");
		salariesFictifs.add(s1);

		// on ajoute le salarier a la liste projet fictif
		projetFictif.setSalaries(salariesFictifs);

		//on ajoute la list salaries a projet repository

		projetRepository.save(projetFictif);

		List<SalarieModeleEntity> result = projetService.filtreSalarie(1L);

		assertEquals("Nom Salarie 1", result.get(0).getNom(),"erreur du test equal ");


		// Testez avec un projet inexistant
		assertThrows(ProjetNotFoundException.class, () -> projetService.filtreSalarie(99L),"erreur du test Throws");
	}

	@Test
	@Transactional // pour évityer de faire de la merde dans la BDD
	public void testGetAllProjets() {

		List<ProjetModeleEntity> projetsFictifs = new ArrayList<>();
		ProjetModeleEntity projet1 = new ProjetModeleEntity();
		projet1.setNom("Projet 1");
		projetRepository.save(projet1);
		ProjetModeleEntity projet2 = new ProjetModeleEntity();
		projet2.setNom("Projet numero 2");
		projetRepository.save(projet2);
		projetsFictifs = projetController.getAllProjet();

		assertEquals(2,projetsFictifs.size());

		assertEquals(0,projetsFictifs.indexOf(projet1));
		assertEquals(8,projetsFictifs.get(0).getNom().length());
		assertEquals("Projet 1",projetsFictifs.get(0).getNom());

		assertEquals(1,projetsFictifs.indexOf(projet2));
		assertEquals(15,projetsFictifs.get(1).getNom().length());
		assertEquals("Projet numero 2",projetsFictifs.get(1).getNom());

	}




	/**
	 * Teste la méthode addSalarieToProjet du service ProjetService.
	 * Vérifie si un salarié est correctement ajouté à un projet.
	 */
	@Test
	@Transactional
	void testAddSalarieToProjet() throws Exception {
		SalarieModeleEntity salarie = new SalarieModeleEntity();
		salarie.setNom("Test");
		salarie.setPrenom("Test");
		ProjetModeleEntity projet = new ProjetModeleEntity();
		projet.setNom("Projet");
		String attendu = salarie.getNom();

		//Appel de la fonction
		projetService.addSalarieToProjet(salarie, projet);
		String resultat = projet.getSalaries().get(0).getNom();
		assertEquals(attendu, resultat);
	}

	/**
	 * Teste la méthode addSalarieToProjet du service ProjetService avec une exception attendue.
	 * Vérifie si l'ajout d'un salarié déjà existant dans le projet génère une exception.
	 */
	@Test
	@Transactional
	void testAddSalarieToProjetThrowsException() throws Exception{
		SalarieModeleEntity salarie = new SalarieModeleEntity();
		salarie.setNom("Test");
		salarie.setPrenom("Test");
		ProjetModeleEntity projet = new ProjetModeleEntity();
		projet.setNom("Projet");
		//On Ajoute le salarie
		projetService.addSalarieToProjet(salarie, projet);
		//Puis on le rajoute pour lever l'exception
		assertThrows(SalarieAppartientProjetException.class, () -> projetService.addSalarieToProjet(salarie, projet));
	}

	/**
	 * Teste la méthode removeSalarieFromProjet du service ProjetService.
	 * Vérifie si un salarié est correctement supprimé d'un projet.
	 */
	@Test
	@Transactional
	void testRemoveSalarieToProjet() throws Exception {
		SalarieModeleEntity salarie = new SalarieModeleEntity();
		salarie.setNom("Test");
		salarie.setPrenom("Test");
		ProjetModeleEntity projet = new ProjetModeleEntity();
		projet.setNom("Projet");
		List<SalarieModeleEntity> salaries = new ArrayList<>();
		salaries.add(salarie);
		projet.setSalaries(salaries);

		//Appel de la fonction
		projetService.removeSalarieFromProjet(salarie, projet);

		assertEquals(projet.getSalaries().size(), 0);
	}

	/**
	 * Teste la méthode removeSalarieFromProjet du service ProjetService avec une exception attendue.
	 * Vérifie si la suppression d'un salarié inexistant dans le projet génère une exception.
	 */
	@Test
	@Transactional
	void testRemoveSalarieToProjetThrowsException() throws Exception {
		SalarieModeleEntity salarie = new SalarieModeleEntity();
		salarie.setNom("Test");
		salarie.setPrenom("Test");
		ProjetModeleEntity projet = new ProjetModeleEntity();
		projet.setNom("Projet");

		assertThrows(SalarieNotFoundException.class, () -> projetService.removeSalarieFromProjet(salarie, projet));
	}
}
